;-----------------------------------
;	App: Geometric Companion		;
;	Version: 0.9					;
;-----------------------------------;
;	Date: 22 May 2017				;
;	Author: Jose Madureira			;
;-----------------------------------;


; ## Global constants ##
STR_MAX_SZ	EQU 21		;Max string buffer size
TAB			EQU 09h		;Tab
NL			EQU 0Dh,0Ah	;New line
TYP		EQU TYPE RAX	;64 bit Type

;USING EXTERNAL PRINTF TO DISPLAY FLOATING POINT
includelib msvcrt.lib
EXTRN printf:PROC

; ## External prototypes ##
ExitProcess PROTO
ReadInt64 PROTO
WriteInt64 PROTO
WriteHex64 PROTO
ReadString PROTO
WriteString PROTO
Crlf PROTO

.data
;	Menu
	msgMenu1 byte	NL,TAB,"********************************",
					NL,TAB,"*     Geometric Companion      *",
					NL,TAB,"*           Main Menu          *",
					NL,TAB,"* which will i aid you with?   *",
					NL,TAB,"********************************",
					NL,TAB,"   (1) Circle",
					NL,TAB,"   (2) Triangle",
					NL,TAB,"   (3) Quadrilateral",0
	msgMenu2 byte	NL,TAB,"   (4) Sphere",
					NL,TAB,"   (5) Cilinder",
					NL,TAB,"   (6) Ellipse",
					NL,TAB,"   (7) Pyramid",
					NL,TAB,"   (0) I'm done, quit please",0
	
	msgMenuOpt byte NL,NL,TAB,">>choice: ",0


;	General Messages 
	msgNewInt byte NL,TAB," /!\ Invalid integer, please enter new one: ", 0
	msgPE byte " Please enter",0
	msgYE byte " You entered: ",0
	msgRpt byte " Repeat ? ",0
	msgNr byte ">>number: ",0
	msgStr byte ">>text: ",0
	divisor byte NL,"------------------------------------------------",NL,0

;	Data Input Messages
	peLength byte TAB,"...length: ",0
	peWidth	byte TAB,"...width: ",0
	peHeight byte TAB,"...height: ",0
	peRadius byte TAB,"...radius: ",0
	peMajorAxis byte TAB,"...major axis (A): ",0
	peMinorAxis byte TAB,"...minor axis (B): ",0
	peSideA byte TAB,"...side A(smallest): ",0
	peSideB byte TAB,"...side B(middle): ",0
	peSideC byte TAB,"...side C(largest): ",0
	peSmallestAngle byte TAB,"...smallest angle: ",0

;	Post-calculation Messages
	msgTriType byte "equilateral",0
			   byte "right",0				;+12 bytes
			   byte "isosceles",0			;+18 bytes

	msgQuadType byte "square",0
				byte "rhombus",0			;+7 bytes
				byte "rectangle",0			;+15 bytes
				byte "parallelogram",0		;+25 bytes

	msgQuadOut byte NL,TAB,"Shape: %s",
					NL,TAB,"Area : %.2f",
					NL,TAB,"Perim: %d",0

	msgTriOut byte	NL,TAB,"Shape: %s",
					NL,TAB,"Area : %.2f",
					NL,TAB,"Perim: %d",0

	msgCircleOut byte	NL,TAB,"Area: %.2f",
						NL,TAB,"Circumference : %.2f",
						NL,TAB,"Diameter: %d",0

	msgEllipseOut byte	NL,TAB,"Area: %.2f",
						NL,TAB,"Circumference : %.2f",0

	msgCilinderOut byte	NL,TAB,"Base Area: %.2f",
						NL,TAB,"Surface Area : %.2f",
						NL,TAB,"Volume: %.2f",0

	msgSphereOut byte	NL,TAB,"Diameter: %d",
						NL,TAB,"Surface Area : %.2f",
						NL,TAB,"Volume: %.2f",0

	msgPyramidOut byte	NL,TAB,"Volume: %.2f",
						NL,TAB,"Base Area: %d",
						NL,TAB,"Surface Area: %.2f",0

;	Data Containers
	xFlt real8 ?		;temporary float storage variable
	xStr byte STR_MAX_SZ DUP (?)	;temporary string

	;Quadrilateral: Type(0=square, 7=rect, 15=rhom, 25=para), Area, Perimeter, radians
	quadData real8 4 DUP(?)

	;Triangle: Type(0=eqi, 12=right, 17=iso), Area, Perimeter
	triData real8 3 DUP(?)

	;Circle: Area, Circumference, Diameter
	circleData real8 3 DUP(?)

	;Ellipse: Area, Circumference
	ellipseData real8 2 DUP(?)

	;Cilinder: Base Area, Surface Area, Volume
	cilinderData real8 3 DUP(?)

	;Sphere: Diameter, Surface Area, Volume
	sphereData real8 3 DUP(?)

	;Pyramid: Volume, Base Area, Surface Area
	pyramidData real8 3 DUP(?)


.code

Main PROC

menu:
	;menu split in two parts due to limitations
	mov rdx,offset msgMenu1			;first part of menu
	call WriteString
	mov rdx,offset msgMenu2			;second part of menu	
	call WriteString
	
	mov rdx,offset msgMenuOpt		;">>choice: "
	call writeString
	call ReadInt64					;get user choice
	call Crlf

	;jump to respective label
	cmp rax,0
	je finish		;quit program

	cmp rax,1
	je circle
	
	cmp rax,2
	je triangle
	
	cmp rax,3
	je quad
	
	cmp rax,4
	je sphere
	
	cmp rax,5
	je cilinder
	
	cmp rax,6
	je ellipse
	
	cmp rax,7
	je pyramid


pyramid:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peLength			;"length: "
	call WriteString
	call ReadInt64
	push rax;prevent overwrite
	
	mov rdx,offset peWidth			;"width: "
	call WriteString
	call ReadInt64
	mov rbx,rax
	
	mov rdx,offset peHeight			;"width: "
	call WriteString
	call ReadInt64
	mov rcx,rax
	pop rax ;retrieve length
	
	;calculate
	mov rdx,offset pyramidData
	call PyramidCalc

;PYRAMID printf() 'c' call
	mov rax, 3						;nr of arguments
	mov rcx,offset msgPyramidOut	;messsage with format
	mov rdx,[pyramidData]			;volume
	mov r8,pyramidData[TYP]			;base area
	mov r9,pyramidData[TYP*2]		;surface area
	call Crlf
	call PrintfMe

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

sphere:	
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peRadius			;"radius: "
	call WriteString
	call ReadInt64

	;calculate	
	mov rdx,offset sphereData
	call SphereCalc

;SPHERE printf() 'c' call
	mov rax, 3						;nr of arguments
	mov rcx,offset msgSphereOut		;messsage with format
	mov rdx,[sphereData]			;diameter
	mov r8,sphereData[TYP]			;surface area
	mov r9,sphereData[TYP*2]		;volume
	call Crlf
	call PrintfMe

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

cilinder:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peRadius			;"radius: "
	call WriteString
	call ReadInt64
	push rax ;prevent overwrite

	mov rdx,offset peHeight			;"height: "
	call WriteString
	call ReadInt64
	mov rbx,rax
	pop rax	;restore radius

	;calculate
	mov rdx,offset cilinderData
	call CilinderCalc

;CILINDER printf() 'c' call
	mov rax, 3						;nr of arguments
	mov rcx,offset msgCilinderOut	;messsage with format
	mov rdx,[cilinderData]			;base area
	mov r8,cilinderData[TYP]		;surface area
	mov r9,cilinderData[TYP*2]		;volume
	call Crlf
	call PrintfMe

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

ellipse:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peMajorAxis		;"major axis(A): "
	call WriteString
	call ReadInt64
	push rax ;prevent overwrite

	mov rdx,offset peMinorAxis		;"minor axis(B): "
	call WriteString
	call ReadInt64
	mov rbx,rax
	pop rax	;restore minor axis

	;calculate
	mov rdx,offset ellipseData
	call EllipseCalc

;ELLIPSE printf() 'c' call
	mov rax, 2						;nr of arguments
	mov rcx,offset msgEllipseOut    ;messsage with format
	mov rdx,[ellipseData]			;area
	mov r8,ellipseData[TYP]			;perimeter
	call Crlf
	call PrintfMe

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

circle:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peRadius			;"radius: "
	call WriteString
	call ReadInt64

	;calculate
	mov rdx,offset circleData
	call CircleCalc

;CIRCLE printf() 'c' call
	mov rax, 3						;nr of arguments
	mov rcx,offset msgCircleOut     ;messsage with format
	mov rdx,[circleData]			;area
	mov r8,circleData[TYP]			;perimeter
	mov r9,circleData[TYP*2]		;diameter
	call Crlf
	call PrintfMe
	
	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

triangle:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peSideA			;"side A (smallest): "
	call WriteString
	call ReadInt64
	mov r8,rax
	
	mov rdx,offset peSideB			;"side B (middle): "
	call WriteString
	call ReadInt64
	mov r9,rax
	
	mov rdx,offset peSideC			;"side C (largest): "
	call WriteString
	call ReadInt64
	mov r10,rax
	
	;calculate
	mov rdx, offset triData
	call TriCalc

;TRIANGLE printf() 'c' call
	mov r13,[triData]
	mov rax, 3						;nr of arguments
	mov rcx,offset msgTriOut        ;messsage with format
	lea rdx,[msgTriType+r13]		;shape
	mov r8,triData[TYP]				;area
	mov r9,triData[TYP*2]			;perimeter
	call Crlf
	call PrintfMe

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

quad:
	call Crlf
	
	;get values
	mov rdx,offset msgPE			;"Please enter..."
	call WriteString
	call Crlf
	
	mov rdx,offset peLength			;"length: "
	call WriteString
	call ReadInt64
	mov r8,rax
	
	mov rdx,offset peWidth			;"width: "
	call WriteString
	call ReadInt64
	mov r9,rax
	
	mov rdx,offset peSmallestAngle	;"smallest angle: "
	call WriteString
	call ReadInt64
	
	;calculate
	mov rdx, OFFSET quadData
	call QuadriCalc

;QUADRILATERAL printf() 'c' call
	mov r13,[quadData]				;byte offset for sub-string

	mov rax, 3						;nr of arguments
	mov rcx,offset msgQuadOut		;messsage with format
	lea rdx,[msgQuadType+r13]		;shape
	mov r8,quadData[TYP]			;area
	mov r9,quadData[TYP*2]			;perimeter

	call Crlf
	call PrintfMe;

	mov rdx,offset divisor			;show divisor line
	call WriteString
	jmp menu						;back to main menu

finish:
	call Crlf
	call Crlf
	call ExitProcess
Main ENDP

; ***********************************************
BoundInt PROC uses RDX
; * Purpose: if integer not between two bounds,	*
; *		then ask for a new one					*
; *---------------------------------------------*
; *  IN : RAX, integer							*
; *		  R8, lower limit						*
; *		  R9, upper limit						*
; *	 Out: RAX, bounded integer					*
; ***********************************************
start:
	cmp RAX, R8
	jl fetch			;if int < lower limit, get new int

	cmp RAX, R9
	jg fetch			;if int > upper limit, get new int

	jmp good			;integer is between two bounds
fetch:
	mov RDX, OFFSET msgNewInt	;" /!\ Invalid integer, please enter new one: "
	call WriteString

	call ReadInt64		;get new integer
	jmp start			;check if integer is bounded
good:
	ret
BoundInt ENDP


; ***********************************************
Deg2Rad64 PROC uses RBX
; * Purpose: convert degrees to radians			*
; *				RAD= (Deg*Pi)/180				*
; *---------------------------------------------*
; *  IN : R13, degree							*
; *  OUT: RAX, radians							*
; ***********************************************
	mov RBX, 180
	push R13
	push RBX
	
	fild qword ptr[rsp]			; st: 180
	fild qword ptr[rsp+TYP]		; st: deg,180
	fldpi						; st: pi,deg,180

	fmulp						; pi*deg
	fxch st(1)					; reverse divisor/divident
	fdivp						; (pi*deg)/180
	fstp xflt					;store float

	;free the stack
	pop RBX
	pop R13

	mov RAX, xflt				;RAX= radians
	ret
Deg2Rad64 ENDP


; ***********************************************
Rad2Deg64 PROC uses RBX
; * Purpose: convert radians to degree			*
; *				Deg= (Rad*180)/Pi				*
; *---------------------------------------------*
; *  IN : R13, radian							*
; *  OUT: RAX, degree							*
; ***********************************************
	mov rbx, 180

	push r13
	push rbx
	fild qword ptr[rsp]			;st: 180
	fld qword ptr[rsp+TYP]		;st: rad, 180
	fmulp						;st: rad*180
	fldpi						;st: pi, (rad*180)
	fdivp						;st: (rad*180)/pi
	;fstp qword ptr[rsp]
	fistp qword ptr[rsp+TYP]		;round to nearest degree
	pop rax
	pop rax
	ret
Rad2Deg64 ENDP


; ***************************************************
ArcCos64 PROC
; * Purpose: finds the arccos of a radian using		*
; *	ArcCos (x) = 2.arctan (sqrt (1-x^2) / (1 + x))	*	
; *-------------------------------------------------*
; *  IN : R13, radian								*
; *  OUT: RAX, ArcCos(R13)							*
; ***************************************************
	push r13
	fld qword ptr[rsp]		;st: x
	fld1					;st: 1, x
	faddp					;st: 1+x
	fld1					;st: 1, 1+x
	fld qword ptr[rsp]		;st: x, 1, 1+x
	fmul st(0), st(0)		;st: x^2, 1, 1+x	
	fsubp					;st: 1-x^2, 1+x
	fsqrt					;st: sqrt(1-x^2), 1+x
	fxch					;st: 1+x, sqrt(1-x^2)
	fpatan					;st: ArcTan(sqrt(1-x^2) / 1+x)
	
	mov rax, 2
	push rax
	fild qword ptr[rsp]		;st: 2, ArcTan(sqrt(1-x^2) / 1+x)
	fmulp					;st: 2*Arctan(sqrt(1-x^2) / 1+x)
	fstp qword ptr[rsp+TYP]	;store in stack

	pop rax
	pop rax					;Arctan(R13)
ret
ArcCos64 ENDP


; *******************************************************************
PrintfMe PROC uses RAX
; * Purpose: display a 'c' style formated message and arguments		*
; *-----------------------------------------------------------------*
; * IN : RAX, number of arguments									*
; *		 RCX, message with format flags								*
; *		 RDX...R15, arguments for formatted message					*
; * OUT: none														*
; *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
; * implementation idea:											*
; *		http://cx20.main.jp/blog/hello/2012/01/04/hello-masm-world	*
; *******************************************************************
	;prevent disaster
	push RBP
	mov RBP, RSP

	imul RAX, 8			;allocate additinal 8 bytes per argument 
	add RAX, 64			;(8*args) + 64 bytes
	mov [RBP], RAX		;save total allocated local space 	
	sub RSP, RAX		;allocate local stack space

	call printf			;display formated message

    add RSP, [RBP]		;clean the stack
	pop RBP

	ret
PrintfMe ENDP


;************************************************
QuadriCalc PROC uses RAX RBX RCX R10 R13
; * Purpose: identify quadilateral figures		* 
; *			 calculate area and perimeter		*
; *---------------------------------------------*
; *  IN : R8, length							*
; *		  R9, width								*
; *		  RAX, smallest angle					*
; *		  RDX, Real8 array offset (4 ele)		*
; *  OUT: none									*
; ***********************************************
	;save regs
	push r9
	push r8
	;;ensure small angle is between 1 and 90 deg
	mov r8, 1
	mov r9, 90
	call BoundInt	;;IN: rax=angle,r8=low bound,r9=high bound
					;;OUT: rax=valid angle
	pop r8
	pop r9

	;convert to radians
	mov r13,rax				;valid degree
	call Deg2Rad64	;;IN: r13=deg, OUT: rax=radian
	mov [rdx+TYP*4],rax		;store raidians

;Determine if equilateral (length=width)
	cmp r8, r9
	jne notEqui

equi:
	cmp r13, 90		;is inner angle 90 deg?
	je square
	jne rhomb
	
	square:		;width = lentgh AND angle = 90 --> square
		;shape
		mov real8 ptr[rdx], 0
	
		;area
		mov rax,r8						;length	
		mul r9							;length * width
		mov qword ptr[rdx+TYP],rax		;store area

		;perimeter
		imul rax,r8, 4					;4*side
		mov qword ptr[rdx+TYP*2],rax	;store perimeter
		jmp done

	rhomb:		;width = length AND angle < 90 --> rhombus
		;shape
		mov qword ptr [rdx], 7

		;area
		push r8
		fld real8 ptr[rdx+TYP*4]	; st: radian
		fsin						; st: sin(randian)
		fild qword ptr[rsp]			; st: side, sin(radian)
		fmul st(0), st(0)			; st: side^2, sin(radian)
		fmulp st(1), st(0)			; st: (side^2)*sin(radian)
		fstp real8 ptr[rdx+TYP]	;store area
		pop r8

		;perimeter
		imul rax,r8, 4				;4*side
		mov [rdx+TYP*2],rax		;store perimeter
		jmp done

notEqui:
	cmp r13, 90		;is inner angle 90 deg?
	je rect
	jne para

	rect:		;width != length AND angle = 90 --> rectangle
		;shape
		mov real8 ptr[rdx], 15

		;area
		mov rax,r8					;length
		mul r9						;length*width
		mov real8 ptr[rdx+TYP],rax	;store area

		;perimeter
		imul rax,r8, 2				;2*length
		imul rcx,r9, 2				;2*width
		add rax, rcx				;2(length+width)
		mov [rdx+TYP*2],rax			;store perimeter
		jmp done

	para:		;width != length AND angle < 90 --> paralellogram
		;shape
		mov qword ptr [rdx], 25
	
		;area = (A*B) * sin(angle)
		mov rax,r8						;length
		imul rax,r9						;length*width

		push rax
		fld real8 ptr[rdx+TYP*4]
		fsin							; st: sin(radian)
		fild qword ptr[rsp]				; st: (len*wid), sin(radian)
		fmulp							; st: (len*wid)*sin(radian)
		fstp real8 ptr[rdx+TYP]		;store area
		pop rax

		;perimeter
		imul rax,r8, 2				;2*length
		imul rcx,r9, 2				;2*width
		add rax,rcx					;2(length+width)
		mov [rdx+TYP*2], rax		;store perimeter
done:
	ret
QuadriCalc ENDP

; ***********************************************
SSSAngle64 PROC uses R8 R9 R10 R13
; * Purpose: find angle with 3 triangle sides   *
; *---------------------------------------------*
; *  IN : R8,R9,R10								*
; *  OUT: RAX,degrees							*
; ***********************************************
;General opposite angle formula:	
	;ArcCos(r8)= (r9^2 + r10^2 - r8^2) / 2(r9*r10)

	imul rax,r9, 2	;rax= 2*r9
	imul rax,r10	;rax= 2(r9*r10)

	imul r8,r8		;r8= r8^2
	imul r9,r9		;r9= r9^2
	imul r10,r10	;r10= r10^2

	add r9,r10		;r9= r9^2 + r10^2
	sub r9,r8		;r9= r9^2 + r10^2 - r8^2

	push rax
	push r9
	fild qword ptr[rsp]			;st: r9
	fild qword ptr[rsp+TYP]		;st: rax,r9
	fdivp						;st: r9/rax
	fstp qword ptr[rsp+TYP]		;save on stack
	
	pop r9
	pop rax						;angle in radians

	;find the ArcCos
	mov r13,rax
	call ArcCos64

	;convert to degree
	mov r13,rax
	call Rad2Deg64

	ret
SSSAngle64 ENDP


; ***********************************************
TriCalc PROC uses RAX RBX RCX
; * Purpose: Triangle Calculations				*
; *---------------------------------------------*
; *	 IN : R8,R9,R10: Sides A, B, C				*
; *		  RDX: Real8 array offset (4 ele)
; *	 OUT: none									*
; ***********************************************

;!!!!!
;Abandoned angle calculation, conflict after first angle is found
;!!!!!
comment !
	mov r8,6			;find opposite angle to this side
	mov r9,8
	mov r10,7
;temporary storage
	mov rbx,r8
	mov rcx,r9
	mov rdx,r10
;Find angles
	;cos(A) =  (b^2 + c^2 - a^2) / 2bc
	call SSSAngle64
	call Crlf
	call WriteInt64

	;cos(B) =  (c^2 + a^2 - b^2) / 2ca
	mov r8,rcx
	mov r9,rdx
	mov r10,rbx
	call SSSAngle64
	call Crlf
	call WriteInt64

	;cos(C) =  (a^2 + b^2 - c^2) / 2ab
	mov r8,rdx
	mov r9,rbx
	mov r10,rcx
	call SSSAngle64
	call Crlf
	call WriteInt64
!

;Determine triangle shape
	mov rax,r8
	imul rax,rax	;A^2
	
	mov rbx,r9
	imul rbx,rbx	;B^2

	mov rcx,r10
	imul rcx,rcx	;C^2

	add rax,rbx		;rax= A^2 + B^2

	cmp rax,rcx		; (A^2 + B^2) ? C^2 
	je right

equi:
	cmp r8,r9		; is A=B ?
	jne isos
	cmp r9,r10		; is A=C?
	jne isos	

	mov real8 ptr[rdx], 0		;set equi shape
	jmp calcs

right:
	cmp r8,r9		; is A=B, then possible equilateral
	je equi

	mov real8 ptr[rdx], 12		;set right shape
	jmp calcs
isos:
	mov real8 ptr[rdx], 18		;set iso shape
	jmp calcs

calcs:
;Area by Heron's Formula
	;Calculate S= (A+B+C)/2
	push r8
	push r9
	push r10
	fild qword ptr[rsp]			;st: C
	fild qword ptr[rsp+TYP]		;st: B, C
	fild qword ptr[rsp+TYP*2]	;st: A, B, C
	faddp						;st: A+B, C
	faddp						;st: A+B+C
	mov rax,2
	push rax
	fild qword ptr[rsp]			;st: 2, A+B+C
	fdivp						;st: S=(A+B+C)/2
	fstp qword ptr[rsp]			;store in stack
	
	;Calculate Area= sqrt[S*(S-A)*(S-B)*(S-C)]
	fld qword ptr[rsp]			;st: S
	fild qword ptr[rsp+TYP*3]	;st: C, S
	fsubp st(1), st(0)			;st: (S-C)
	
	fld qword ptr[rsp]			;st: S, (S-C)
	fild qword ptr[rsp+TYP*2]	;st: B, S, (S-C)
	fsubp st(1), st(0)			;st: (S-B), (S-C)

	fld qword ptr[rsp]			;st: S, (S-B), (S-C)
	fild qword ptr[rsp+TYP]		;st: A, S, (S-B), (S-C)
	fsubp st(1), st(0)			;st: (S-A), (S-B), (S-C)

	fld qword ptr[rsp]			;st: S, (S-A), (S-B), (S-C)
	fmulp						;st: S*(S-A), (S-B), (S-C)
	fmulp						;st: S*(S-A)*(S-B), (S-C)
	fmulp						;st: S*(S-A)*(S-B)*(S-C)
	fsqrt						;st: sqrt[S*(S-A)*(S-B)*(S-C)]

	fstp real8 ptr[rdx+TYP]

	pop rax
	pop r10
	pop r9
	pop r8
	
;Perimeter: A+B+C
	mov rax, r8		;A
	add rax, r9		;A+B
	add rax, r10	;A+B+C
	mov [rdx+TYP*2],rax

	ret
TriCalc ENDP


; ***********************************************
CircleCalc PROC uses RAX RBX
; * Purpose: Circle calculator					*
; *---------------------------------------------*
; *  IN : RAX, radius							*
; *		  RDX, Real8 array offset (3 ele)		*
; *  OUT: none									*
; ***********************************************
	;Area: PI *(R^2)
	push rax
	fild qword ptr[rsp]		;st: r
	fmul st(0),st(0)		;st: r^2
	fldpi					;st: pi, r^2
	fmulp					;st: pi * r^2
	fstp real8 ptr[rdx]		;store area
	pop rax

	;Circumference: 2PI * R
	mov rbx,2
	push rax
	push rbx
	fild qword ptr[rsp+TYP]		;st: r
	fild qword ptr[rsp]		    ;st: 2, r
	fldpi						;st: pi, 2, r
	fmulp						;st: 2pi, r
	fmulp						;st: 2pi*r
	fstp real8 ptr[rdx+TYP]		;store circumference
	pop rbx
	pop rax

	;Diameter: 2R
	imul rax, 2
	mov real8 ptr[rdx+TYP*2], rax

ret
CircleCalc ENDP


; ***********************************************
EllipseCalc PROC
; * Purpose: ellipse calculator					*
; *---------------------------------------------*
; *  IN : RAX, major axis (A)					*
; *		  RBX, minor axis (B)					*
; *		  RDX, Real8 array offset (3 ele)		*
; *  OUT: none									*
; ***********************************************
	;Area: PI * A *B
	push rax
	push rbx
	fild qword ptr[rsp]			;st: B
	fild qword ptr[rsp+TYP]		;st: A, B
	fldpi						;st: pi, A, B
	fmulp						;st: pi*A, B
	fmulp						;st: pi*A*B
	fstp real8 ptr[rdx]			;store area
	pop rbx
	pop rax

	;Circumference: PI * Sqrt[ 2(A^2+B^2) - ((A-B)^2) /2) ]
	push rbx
	push rax
	fild qword ptr[rsp]			;st: A
	fild qword ptr[rsp+TYP]		;st: B, A
	fsubp						;st: A-B
	fmul st(0), st(0)			;st: (A-B)^2
	fld1						;st: 1, (A-B)^2
	fadd st(0), st(0)			;st: 2, (A-B)^2
	fdivp						;st: (A-B)^2 /2
	fild qword ptr[rsp]			;st: A, (A-B)^2 /2
	fmul st(0), st(0)			;st: A^2, (A-B)^2 /2
	fild qword ptr[rsp+TYP]		;st: B, A^2, (A-B)^2 /2
	fmul st(0), st(0)			;st: B^2, A^2, (A-B)^2 /2
	faddp						;st: (B^2 + A^2), (A-B)^2 /2	
	fld1						;st: 1, (B^2 + A^2), (A-B)^2 /2
	fadd st(0), st(0)			;st: 2, (B^2 + A^2), (A-B)^2 /2
	fmulp						;st: 2*(B^2 + A^2), (A-B)^2 /2
	fxch						;st: (A-B)^2 /2, 2*(B^2 + A^2)
	fsubp						;st: 2*(B^2 + A^2) - ((A-B)^2 /2)
	fsqrt						;st: sqrt[ 2*(B^2 + A^2) - ((A-B)^2 /2) ]
	fldpi						;st: pi, sqrt[ 2*(B^2 + A^2) - ((A-B)^2 /2) ]
	fmulp						;st: pi * sqrt[ 2*(B^2 + A^2) - ((A-B)^2 /2) ]
	fstp real8 ptr[rdx+TYP]
	pop rax
	pop rbx
ret
EllipseCalc ENDP


; ***********************************************
CilinderCalc PROC
; * Purpose: cilinder calculator				*
; *---------------------------------------------*
; *  IN : RAX, radius							*
; *		  RBX, height							*
; *		  RDX, Real8 array offset (3 ele)		*
; *  OUT: none									*
; ***********************************************
	push rbx
	push rax

	;Base Area: PI * R^2
	fild qword ptr[rsp]		;st: r
	fmul st(0), st(0)		;st: r^2
	fldpi					;st: pi, r^2
	fmulp					;st: pi * r^2
	fstp real8 ptr[rdx]		;store base area

	;Surface Area: (2PI * R * H)  + 2PI *R^2
	fild qword ptr[rsp]		;st: r
	fmul st(0), st(0)		;st: r^2
	fldpi					;st: pi, r^2
	fld1					;st: 1, pi, r^2
	fadd st(0), st(0)		;st: 2, pi, r^2
	fmulp					;st: 2*pi, r^2
	fmulp					;st: (2*pi * r^2)
	fild qword ptr[rsp+TYP]	;st: h, (2*pi * r^2)
	fild qword ptr[rsp]		;st: r, h, (2*pi * r^2)
	fldpi					;st: pi, r, h, (2*pi * r^2)
	fld1					;st: 1, pi, r, h, (2*pi * r^2)
	fadd st(0), st(0)		;st: 2, pi, r, h, (2*pi * r^2)
	fmulp					;st: (2*pi), r, h, (2*pi * r^2)
	fmulp					;st: (2*pi * r), h, (2*pi * r^2)
	fmulp					;st: (2*pi * r *h), (2*pi * r^2)
	faddp					;st: (2*pi * r *h) + (2*pi * r^2)
	fstp real8 ptr[rdx+TYP]	;store surface area
	
	;Volume: PI * R^2 * H
	fild qword ptr[rsp+TYP]	;st: h
	fild qword ptr[rsp]		;st: r, h
	fmul st(0), st(0)		;st: r^2, h
	fldpi					;st: pi, r^2, h
	fmulp					;st: pi * r^2, h
	fmulp					;st: pi * r^2 * h
	fstp real8 ptr[rdx+TYP*2] ;store volume 

	pop rax
	pop rbx


ret
CilinderCalc ENDP


; ***********************************************
SphereCalc PROC
; * Purpose: cilinder calculator				*
; *---------------------------------------------*
; *  IN : RAX, radius							*
; *		  RDX, Real8 array offset (3 ele)		*
; *  OUT: none									*
; ***********************************************
	push rax

	;Diameter: 2R
	imul rax, 2
	mov real8 ptr[rdx],rax		;store Diameter

	;Surface Area: 4PI * R^2
	fild qword ptr[rsp]			;st: r
	fmul st(0), st(0)			;st: r^2
	fldpi						;st: pi, r^2
	mov rax, 4
	push rax
	fild qword ptr[rsp]			;st: 4, pi, r^2
	fmulp						;st: 4*pi, r^2
	fmulp						;st: 4*pi * r^2
	fstp real8 ptr[rdx+TYP]		;store surface area

	;Volume: 4/3(PI) * R^3
	fild qword ptr[rsp+TYP]		;st: r
	fmul st(0), st(0)			;st: r^2
	fild qword ptr[rsp+TYP]		;st: r, r^2
	fmulp						;st: r^3
	fldpi						;st: pi, r^3
	fild qword ptr[rsp]			;st: 4, pi, r^3
	fmul st(1), st(0)			;st: 4, 4*pi, r^3
	fld1						;st: 1, 4, 4*pi, r^3
	fsubp						;st: 3, 4*pi, r^3
	fdivp						;st: 4/3(pi), r^3
	fmulp						;st: 4/3(pi) * r^3
	fstp real8 ptr[rdx+TYP*2]	;store volume

	pop rax
	pop rax
ret
SphereCalc ENDP


; ***********************************************
PyramidCalc PROC
; * Purpose: pyramid calculator					*
; *---------------------------------------------*
; *  IN : RAX, length							*
; *		  RBX, width							*
; *		  RCX, height							*
; *		  RDX, Real8 array offset (3 ele)		*
; *  OUT: none									*
; ***********************************************
	push rax
	push rbx
	push rcx

	;Volume: ( L*W*H )/3
	fild qword ptr[rsp]			;st: h
	fild qword ptr[rsp+TYP]		;st: w, h
	fild qword ptr[rsp+TYP*2]	;st: l, w, h
	fmulp						;st: L*w, h
	fmulp						;st: L*w*h
	mov rax, 3
	push rax
	fild qword ptr[rsp]			;st: 3, (l*w*h)
	pop rax
	fdivp						;st: (l*w*h)/3
	fstp real8 ptr[rdx]			;store volume

	;Base Area: L*W
	fild qword ptr[rsp+TYP]		;st: w
	fild qword ptr[rsp+TYP*2]	;st: l, w
	fmulp						;st: l*w
	fistp real8 ptr[rdx+TYP]	;store base area


	;Surface Area: L*W + L*Sqrt[ (W/2)^2 + H^2 ] + W*Sqrt[ (L/2)^2 + H^2 ]
	fild qword ptr[rsp]			;st: h
	fmul st(0), st(0)			;st: h^2
	fild qword ptr[rsp+TYP*2]	;st: l, h^2
	fld1						;st: 1, l, h^2
	fadd st(0), st(0)			;st: 2, l, h^2
	fdivp						;st: l/2, h^2
	fmul st(0), st(0)			;st: (l/2)^2, h^2
	faddp						;st: (l/2)^2 + h^2
	fsqrt						;st: sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp+TYP]		;st: w, sqrt[(l/2)^2 + h^2]
	fmulp						;st: w*sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp]			;st: h, w*sqrt[(l/2)^2 + h^2]
	fmul st(0), st(0)			;st: h^2, w*sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp+TYP]		;st: w, h^2, w*sqrt[(l/2)^2 + h^2]
	fld1						;st: 1, w, h^2, w*sqrt[(l/2)^2 + h^2]
	fadd st(0), st(0)			;st: 2, w, h^2, w*sqrt[(l/2)^2 + h^2]
	fdivp						;st: w/2, h^2, w*sqrt[(l/2)^2 + h^2]
	fmul st(0), st(0)			;st: (w/2)^2, h^2, w*sqrt[(l/2)^2 + h^2]
	faddp						;st: (w/2)^2 + h^2, w*sqrt[(l/2)^2 + h^2]
	fsqrt						;st: sqrt[(w/2)^2 + h^2], w*sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp+typ*2]	;st: l, sqrt[(w/2)^2 + h^2], w*sqrt[(l/2)^2 + h^2]
	fmulp						;st: l*sqrt[(w/2)^2 + h^2], w*sqrt[(l/2)^2 + h^2]
	faddp						;st: l*sqrt[(w/2)^2 + h^2] + w*sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp+TYP]		;st: w, l*sqrt[(w/2)^2 + h^2] + w*sqrt[(l/2)^2 + h^2]
	fild qword ptr[rsp+TYP*2]	;st: l, w, l*sqrt[(w/2)^2 + h^2] + w*sqrt[(l/2)^2 + h^2]
	fmulp						;st: l*w, l*sqrt[(w/2)^2 + h^2] + w*sqrt[(l/2)^2 + h^2]
	faddp						;st: l*w + l*sqrt[(w/2)^2 + h^2] + w*sqrt[(l/2)^2 + h^2]
	fstp real8 ptr[rdx+TYP*2]	;store surface area


	pop rax
	pop rbx
	pop rcx

ret
PyramidCalc ENDP

PUBLIC Main
END
